//
//  UserDefaultManager.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/19.
//

import RealmSwift

/**
 * Singletone
 */

class FavoriteMovie: Object {
    @objc dynamic var id: String = ""               // ID
    @objc dynamic var link: String = ""             // 영화 링크
    @objc dynamic var title: String = ""            // 영화 제목
    @objc dynamic var director: String = ""         // 영화 감독
    @objc dynamic var actors: String = ""           // 영화 출연진
    @objc dynamic var rating: String = ""           // 영화 평점
    @objc dynamic var imaegeUrlString: String = ""  // 영화 포스터 이미지
    
    static let KEY_ID = "id"
    
    public required convenience init(link: String,
                                     imageUrlString: String,
                                     title: String,
                                     director: String,
                                     actors: String,
                                     rating: String) {
        self.init()
        setupItem(id: title + link, link: link, imageUrlString: imageUrlString, title: title, director: director, actors: actors, rating: rating)
    }
    
    private func setupItem(id: String,
                           link: String,
                           imageUrlString: String,
                           title: String,
                           director: String,
                           actors: String,
                           rating: String) {
        self.id = id
        self.link = link
        self.imaegeUrlString = imageUrlString
        self.title = title
        self.director = director
        self.actors = actors
        self.rating = rating
    }
    
    override class func primaryKey() -> String? {
        return FavoriteMovie.KEY_ID
    }
    
}

// MARK: - Support Methods
extension FavoriteMovie {
    func create() throws {
        do {
            let realm = try Realm()
            try realm.write {
                realm.create(FavoriteMovie.self,
                             value: self)
            }
        } catch {
            throw error
        }
    }
    // 즐겨찾는 영화 가져오기
    static func getFavoriteMovieBy(id: String) throws -> FavoriteMovie? {
        do {
            let realm = try Realm()
            
            return realm.objects(FavoriteMovie.self)
                .filter(NSPredicate(format: "\(FavoriteMovie.KEY_ID) == %@", argumentArray: [id]))
                .first
        } catch {
            throw error
        }
    }
    
    // 즐겨찾는 영화리스트 가져오기
    static func getFavorite() throws -> [FavoriteMovie] {
        do {
            let realm = try Realm()
            return realm.objects(FavoriteMovie.self)
                .map { $0 }
        } catch {
            throw error
        }
    }
    
    // 리스트 업데이트
    static func updateFavoriteMovies(favoriteMovies: [FavoriteMovie],
                                       _ completion: @escaping (Bool) -> ()) {
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(favoriteMovies, update: .modified)
            }
            completion(true)
        } catch {
            completion(false)
        }
    }
    
    // 즐겨찾기한 영화 체크
    static func isFavorite(id: String) -> Bool {
        do {
            let realm = try Realm()
            let object = realm.objects(FavoriteMovie.self)
                .filter(NSPredicate(format: "\(FavoriteMovie.KEY_ID) == %@", argumentArray: [id]))
                .first
            guard let _ = object else {
                return false
            }
            
            return true
        } catch {
            return false
        }
    }
    
    // 영화 삭제
    static func deleteBy(id: String) throws {
        do {
            let realm = try Realm()
            try realm.write {
                let favoriteMovies = realm.objects(FavoriteMovie.self)
                    .filter(NSPredicate(format: "\(FavoriteMovie.KEY_ID) == %@", argumentArray: [id]))
                realm.delete(favoriteMovies)
            }
        } catch {
            throw error
        }
    }
}
