//
//  SearchAPI.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/18.
//

import Moya

enum SearchAPI {
    case movieSearch(keyword: String, page: Int)
}

extension SearchAPI: TargetType {
    var baseURL: URL {
        return URL(string: "https://openapi.naver.com/v1")!
    }
    
    var path: String {
        switch self {
        case .movieSearch(_, _):
            return "/search/movie.json"
        }
    }
    
    var method: Method {
        switch self {
        case .movieSearch(_, _):
            return .get
        }
    }
    
    var task: Task {
        switch self {
        case .movieSearch(let keyword, let page):
            let params: [String: Any] = [
                "query": keyword,
                "display": 10,
                "start": page
            ]
            
            return .requestParameters(parameters: params,
                                      encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return [
            "X-Naver-Client-Id": "D4KpoLAlBBfd02LYlLbn",
            "X-Naver-Client-Secret": "jZj6z2KPO3",
            "Content-type": "application/json"
        ]
    }
}
