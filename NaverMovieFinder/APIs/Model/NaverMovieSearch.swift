//
//  NaverMovieSearch.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/18.
//

import Foundation

struct NaverMovieSearch: Codable {
    let total: Int?
    let start: Int?
    let display: Int?
    let lastBuildDate: String?
    let items: [Movie]
}

struct Movie: Codable {
    let title: String?
    let link: String?
    let image: String?
    let subtitle: String?
    let pubDate: String?
    let director: String?
    let actor: String?
    let userRating: String?
}
