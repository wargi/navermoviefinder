//
//  SearchMovieTableViewCell.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/18.
//

import UIKit
import Then
import SnapKit
import Kingfisher
//MARK: 영화 정보 cell
final class SearchMovieTableViewCell: UITableViewCell {
    //MARK: - Properties
    static let identifier = "SearchMovieTableViewCell"
    
    // 영화 포스터 이미지
    let posterImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFit
    }
    // 영화 제목
    let movieTitleLabel = UILabel().then {
        $0.textAlignment = .left
        $0.font = .systemFont(ofSize: 16, weight: .bold)
    }
    // 즐겨찾기 버튼
    let favoriteButton = UIButton().then {
        $0.setImage(UIImage(named: "iconStarEmpty"), for: .normal)
        $0.setImage(UIImage(named: "iconStarFilled"), for: .selected)
    }
    // 영화 감독
    let directorLabel = UILabel().then {
        $0.textAlignment = .left
        $0.font = .systemFont(ofSize: 13, weight: .regular)
    }
    // 출연진
    let actorsLabel = UILabel().then {
        $0.textAlignment = .left
        $0.font = .systemFont(ofSize: 13, weight: .regular)
    }
    // 평점
    let userRatingLabel = UILabel().then {
        $0.textAlignment = .left
        $0.font = .systemFont(ofSize: 13, weight: .regular)
    }
    // 빈 뷰
    let emptyView = UIView()
    
    //MARK: - Initializer
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // override
    override func prepareForReuse() {
        super.prepareForReuse()
        
        posterImageView.image = nil
    }
    
    //MARK: - Configure UI
    func configureUI() {
        selectionStyle = .none
        
        contentView.addSubview(posterImageView)
        contentView.addSubview(favoriteButton)
        contentView.addSubview(movieTitleLabel)
        contentView.addSubview(directorLabel)
        contentView.addSubview(actorsLabel)
        contentView.addSubview(userRatingLabel)
        contentView.addSubview(emptyView)
        
        posterImageView.snp.makeConstraints {
            $0.top.equalToSuperview().inset(10)
            $0.left.equalToSuperview().offset(16)
            $0.width.equalTo(60)
            $0.height.equalTo(90)
        }
        
        favoriteButton.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.right.equalToSuperview()
            $0.width.equalTo(30)
            $0.height.equalTo(40)
        }
        
        movieTitleLabel.snp.makeConstraints {
            $0.left.equalTo(posterImageView.snp.right).offset(10)
            $0.top.equalToSuperview().offset(10)
            $0.right.equalTo(favoriteButton.snp.left)
        }
        
        directorLabel.snp.makeConstraints {
            $0.left.equalTo(posterImageView.snp.right).offset(10)
            $0.top.equalTo(movieTitleLabel.snp.bottom).offset(5)
            $0.right.equalToSuperview()
        }
        
        actorsLabel.snp.makeConstraints {
            $0.left.equalTo(posterImageView.snp.right).offset(10)
            $0.top.equalTo(directorLabel.snp.bottom).offset(5)
            $0.right.equalToSuperview()
        }
        
        userRatingLabel.snp.makeConstraints {
            $0.left.equalTo(posterImageView.snp.right).offset(10)
            $0.top.equalTo(actorsLabel.snp.bottom).offset(5)
            $0.right.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints {
            $0.top.equalTo(posterImageView.snp.bottom).offset(10)
            $0.left.right.bottom.equalToSuperview()
        }
    }
    
    //MARK: - Fetch data
    func fetch(movie: Movie) {
        guard let title = movie.title,
              let director = movie.director,
              let actors = movie.actor,
              let rating = movie.userRating,
              let imageUrlString = movie.image else { return }
        
        if let imageUrl = URL(string: imageUrlString) {
            posterImageView.kf.setImage(with: imageUrl)
        }
        
        movieTitleLabel.text = title
        directorLabel.text = "감독: \(director)"
        actorsLabel.text = "출연: \(actors)"
        userRatingLabel.text = "평점: \(rating)"
    }
}
