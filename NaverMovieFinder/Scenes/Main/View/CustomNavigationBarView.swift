//
//  CustomNavigationBarView.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/18.
//

import Foundation
import UIKit
import Then
import SnapKit
//MARK: Custom NavigationBar
final class CustomNavigationBarView: UIView {
    //MARK: - Properties
    // Navigation Title
    let titleLabel = UILabel().then {
        $0.font = .systemFont(ofSize: 20, weight: .bold)
    }
    // Navigation Left Button
    let leftBarButton = UIButton().then {
        $0.isHidden = true
        $0.contentMode = .scaleAspectFit
    }
    // Navigation Right Button
    let rightBarButton = UIButton().then {
        $0.setTitle("즐겨찾기", for: .normal)
        $0.setTitle("즐겨찾기", for: .highlighted)
        $0.setImage(UIImage(named: "iconStarFilled"), for: .normal)
        $0.setImage(UIImage(named: "iconStarFilled"), for: .highlighted)
        $0.setTitleColor(.black, for: .normal)
        $0.titleLabel?.font = .systemFont(ofSize: 15, weight: .medium)
        $0.layer.borderColor = UIColor(named: "lineColor")?.cgColor
        $0.layer.borderWidth = 1
        $0.isHidden = true
    }
    
    //MARK: - Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Configure UI
    func configureUI() {
        addSubview(titleLabel)
        addSubview(leftBarButton)
        addSubview(rightBarButton)
        
        leftBarButton.snp.makeConstraints {
            $0.top.left.equalToSuperview()
            $0.size.equalTo(54)
        }
        
        rightBarButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.right.equalToSuperview().offset(-20)
            $0.right.width.equalTo(82)
            $0.height.equalTo(30)
        }
        
        titleLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.right.equalToSuperview().inset(20)
        }
    }
}
