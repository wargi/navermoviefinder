//
//  MainViewModel.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/18.
//

import Moya
import RxSwift
import RxCocoa
//MARK: - MainViewModel
final class MainViewModel {
    //MARK: Properties
    let input = Input()
    let output = Output()
    private let disposeBag = DisposeBag()
    let searchProvider = MoyaProvider<SearchAPI>()
    private var page = 1
    private var totalPage = 0
    private var movies = [Movie]()
    
    //MARK: Initializer
    init() {
        rxBind()
    }
    //MARK: Rx Binding..
    func rxBind() {
        // Server <= Movie Search
        input.requestSearch
            .bind(with: self, onNext: { owner, keyword in
                owner.reset()
                owner.requestSearch(keyword: keyword)
            })
            .disposed(by: disposeBag)
        // Server <= Movie Search (Paging)
        input.requestAddPage
            .throttle(.milliseconds(500), scheduler: MainScheduler.asyncInstance)
            .bind(with: self, onNext: { owner, keyword in
                owner.page += 10
                guard owner.page <= owner.totalPage && owner.page < 1000 else { return }
                owner.requestSearch(keyword: keyword)
            })
            .disposed(by: disposeBag)
        // Error Handling
        output.error
            .bind(with: self, onNext: { owner, _ in
                owner.reset()
                owner.output.searchResult.accept([])
            })
            .disposed(by: disposeBag)
    }
    
    //MARK: Fuctions..
    // 초기화
    func reset() {
        movies = []
        page = 1
        totalPage = 0
    }
    // => 즐겨찾는 영화
    func fetchIsFavorite(movie: Movie) -> Bool {
        guard let title = movie.title, let link = movie.link else { return false }
        
        return FavoriteMovie.isFavorite(id: title + link)
    }
    // <= 즐겨찾기 추가/삭제
    func requestFavorite(idx: Int) {
        guard movies.count > idx else { return }
        let movie = movies[idx]
        guard let title = movie.title, let link = movie.link else { return }

        do {
            if let _ = try FavoriteMovie.getFavoriteMovieBy(id: title + link) {
                try FavoriteMovie.deleteBy(id: title + link)
            } else {
                let favorite = FavoriteMovie(link: link,
                                             imageUrlString: movie.image ?? "",
                                             title: title,
                                             director: movie.director ?? "",
                                             actors: movie.actor ?? "",
                                             rating: movie.userRating ?? "")
                try favorite.create()
            }
        } catch {
            print(error.localizedDescription)
            output.error.accept(ErrorResult.favorite)
        }
    }
}
//MARK: - Input/Output/Error
extension MainViewModel {
    enum ErrorResult: Error {
        case requestSearch // => Network
        case reseponseSearch // => Response
        case favorite // => Favorite
    }
    
    struct Input {
        let requestSearch = PublishRelay<String?>() // <= 검색
        let requestAddPage = PublishRelay<String?>() // <= 추가 검색
    }
    
    struct Output {
        let error = PublishRelay<ErrorResult>() // => Error
        let searchResult = PublishRelay<[Movie]>() // => 검색 결과
    }
}

//MARK: - Request 요청
extension MainViewModel {
    // 영화 목록 조회
    private func requestSearch(keyword: String?) {
        searchProvider.request(.movieSearch(keyword: keyword ?? "", page: page)) { result in
            switch result {
            case let .success(response):
                guard let result = try? response.map(NaverMovieSearch.self) else {
                    self.output.error.accept(ErrorResult.reseponseSearch)
                    return
                }
                // 전체 페이지
                self.totalPage = result.total ?? 0
                
                // 필요없는 문자 열 제거
                let filterdMovies = result.items.map { movie -> Movie in
                    let filterdTitle = movie.title?.replacingOccurrences(of: "<b>", with: "").replacingOccurrences(of: "</b>", with: "")
                    
                    var director = movie.director
                    var actors = movie.actor
                    
                    if director?.last == "|" { director?.removeLast() }
                    if actors?.last == "|" { actors?.removeLast() }
                    
                    let filterdDirector = director?.replacingOccurrences(of: "|", with: ", ")
                    let filterdActors = actors?.replacingOccurrences(of: "|", with: ", ")
                    
                    return Movie(title: filterdTitle,
                                 link: movie.link,
                                 image: movie.image,
                                 subtitle: movie.subtitle,
                                 pubDate: movie.pubDate,
                                 director: filterdDirector,
                                 actor: filterdActors,
                                 userRating: movie.userRating)
                }
                
                self.movies.append(contentsOf: filterdMovies)
                
                // 받아온 결과 전달
                self.output.searchResult.accept(self.movies)
            case let .failure(error):
                print(error.localizedDescription)
                self.output.error.accept(ErrorResult.requestSearch)
            }
        }
    }
}
