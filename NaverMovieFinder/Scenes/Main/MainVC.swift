//
//  ViewController.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/18.
//

import UIKit
import RxSwift
import RxCocoa
//MARK: 영화 검색 결과 VC
class MainVC: UIViewController {
    //MARK: - Properties
    let viewModel: MainViewModel
    let disposeBag = DisposeBag()
    
    // NavigationBar
    let navigationBar = CustomNavigationBarView().then {
        $0.titleLabel.text = "네이버 영화 검색"
        $0.titleLabel.textAlignment = .left
        $0.rightBarButton.isHidden = false
    }
    // SearchBar
    let searchTextField = UITextField().then {
        $0.borderStyle = .roundedRect
        $0.clearButtonMode = .always
    }
    // Navigation 구분선
    let navigationLineView = UIView().then {
        $0.backgroundColor = UIColor(named: "lineColor") ?? .lightGray
    }
    
    // 검색 겨로가 TableView
    let searchTableView = UITableView().then {
        $0.keyboardDismissMode = .onDrag
        $0.alwaysBounceHorizontal = false
        $0.showsVerticalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
        $0.register(SearchMovieTableViewCell.self, forCellReuseIdentifier: SearchMovieTableViewCell.identifier)
    }
    // 검색 내역이 없을 때 표시할 label
    let emptyLabel = UILabel().then {
        $0.text = "검색 결과가 없습니다."
        $0.textColor = .darkGray
        $0.textAlignment = .center
        $0.font = .systemFont(ofSize: 12, weight: .light)
    }
    
    //MARK: - Initializer
    init(viewModel: MainViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        rxBind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        searchTableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        searchTextField.resignFirstResponder()
    }
    
    //MARK: - Configure UI
    func configureUI() {
        navigationController?.navigationBar.isHidden = true
        
        view.backgroundColor = .white
        
        view.addSubview(navigationBar)
        view.addSubview(navigationLineView)
        view.addSubview(searchTextField)
        view.addSubview(searchTableView)
        view.addSubview(emptyLabel)
        
        navigationBar.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(54)
        }
        
        navigationLineView.snp.makeConstraints {
            $0.top.equalTo(navigationBar.snp.bottom)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(1)
        }
        
        searchTextField.snp.makeConstraints {
            $0.top.equalTo(navigationBar.snp.bottom).offset(10)
            $0.left.right.equalToSuperview().inset(20)
        }
        
        searchTableView.snp.makeConstraints {
            $0.top.equalTo(searchTextField.snp.bottom).offset(10)
            $0.left.bottom.equalToSuperview()
            $0.right.equalToSuperview().offset(-20)
        }
        
        emptyLabel.snp.makeConstraints {
            $0.centerY.equalTo(searchTableView)
            $0.left.right.equalToSuperview()
        }
    }
    
    //MARK: - Rx Binding..
    func rxBind() {
        // Error Handling
        viewModel
            .output
            .error
            .bind(with: self, onNext: { owner, error in
                switch error {
                case .requestSearch:
                    print("네트워크 통신 에러 발생!")
                case .reseponseSearch:
                    print("리스폰스 에러 발생!")
                case .favorite:
                    print("즐겨찾기 추가/삭제 에러 발생!")
                }
            })
            .disposed(by: disposeBag)
        // Server <= 영화 검색
        searchTextField
            .rx
            .text
            .debounce(.milliseconds(500), scheduler: MainScheduler.asyncInstance)
            .bind(with: self, onNext: { owner, keyword in
                owner.viewModel
                    .input
                    .requestSearch
                    .accept(keyword)
            })
            .disposed(by: disposeBag)
        // 내역이 없을 때 내역 없음 label 표시
        viewModel
            .output
            .searchResult
            .map { !$0.isEmpty }
            .bind(to: emptyLabel.rx.isHidden)
            .disposed(by: disposeBag)
        // Server => 검색 결과 리스트 표시
        viewModel
            .output
            .searchResult
            .bind(to: searchTableView.rx.items(cellIdentifier: SearchMovieTableViewCell.identifier,
                                               cellType: SearchMovieTableViewCell.self)) { row, info, cell in
                cell.fetch(movie: info)
                cell.favoriteButton.tag = row
                cell.favoriteButton.addTarget(self, action: #selector(self.cellFavoriteTapped(sender:)), for: .touchUpInside)
                cell.favoriteButton.isSelected = self.viewModel.fetchIsFavorite(movie: info)
            }
            .disposed(by: disposeBag)
        // cell 선택 event handler
        searchTableView
            .rx
            .modelSelected(Movie.self)
            .bind(with: self, onNext: { owner, info in
                // MainVC => DetailVC
                let detailViewModel = DetailViewModel(movie: info)
                let detailVC = DetailVC(viewModel: detailViewModel)
                owner.navigationController?.pushViewController(detailVC, animated: true)
            })
            .disposed(by: disposeBag)
        // searchTableView delegate
        searchTableView
            .rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        // 즐겨찾기 button handler
        navigationBar
            .rightBarButton
            .rx
            .tap
            .bind(with: self, onNext: { owner, _ in
                // MainVC => FavoriteVC
                let favoriteViewModel = FavoriteViewModel()
                let favoriteVC = FavoriteVC(viewModel: favoriteViewModel)
                let navigation = UINavigationController(rootViewController: favoriteVC)
                navigation.modalPresentationStyle = .fullScreen
                owner.present(navigation, animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
    }
    
    //MARK: - override
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        searchTextField.resignFirstResponder()
    }
    // Cell Favorite button tap handler
    @objc
    func cellFavoriteTapped(sender: UIButton) {
        viewModel.requestFavorite(idx: sender.tag)
        
        searchTableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
    }
}

extension MainVC: UITableViewDelegate {
    // Paging
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = searchTableView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let frameHeight = scrollView.frame.height
        
        if offsetY > contentHeight - frameHeight {
            viewModel.input.requestAddPage.accept(searchTextField.text)
        }
    }
}
