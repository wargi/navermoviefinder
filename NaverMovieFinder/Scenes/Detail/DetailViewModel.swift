//
//  DetailViewModel.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/18.
//

import RxSwift
import RxCocoa
//MARK: - DetailViewModel
final class DetailViewModel {
    //MARK: Properties
    private let disposeBag = DisposeBag()
    fileprivate let movie: Movie
    
    //MARK: Initializer
    init(movie: Movie) {
        self.movie = movie
    }
}
//MARK: - Fuctions..
extension DetailViewModel {
    // => 영화 제목
    func fetchTitle() -> Observable<String?> {
        return Observable.just(movie.title)
    }
    // => 영화 감독
    func fetchDirector() -> Observable<String> {
        return Observable.just("감독: \(movie.director ?? "")")
    }
    // => 영화 출연진
    func fetchActors() -> Observable<String?> {
        return Observable.just("출연: \(movie.actor ?? "")")
    }
    // => 영화 평점
    func fetchRating() -> Observable<String?> {
        return Observable.just("평점: \(movie.userRating ?? "")")
    }
    // => 영화 포스터 이미지 url
    func fetchImageUrl() -> Observable<URL?> {
        return Observable.just(URL(string: movie.image ?? ""))
    }
    // => 영화 정보 link
    func fetchRequest() -> Observable<URLRequest?> {
        guard let urlString = movie.link,
              let url = URL(string: urlString) else { return Observable.just(nil) }
          
        return Observable.just(URLRequest(url: url))
    }
    // => 즐겨찾는 영화
    func fetchIsFavorite() -> Observable<Bool> {
        guard let title = movie.title, let link = movie.link else { return Observable.just(false) }
        
        return Observable.just(FavoriteMovie.isFavorite(id: title + link))
    }
    // <= 즐겨찾기 추가/삭제
    func requestFavorite() {
        guard let title = movie.title,
              let link = movie.link else { return }
        
        do {
            if let _ = try FavoriteMovie.getFavoriteMovieBy(id: title + link) {
                try FavoriteMovie.deleteBy(id: title + link)
            } else {
                let favorite = FavoriteMovie(link: link,
                                             imageUrlString: movie.image ?? "",
                                             title: title,
                                             director: movie.director ?? "",
                                             actors: movie.actor ?? "",
                                             rating: movie.userRating ?? "")
                try favorite.create()
            }
        } catch {
            print(error)
        }
    }
}
