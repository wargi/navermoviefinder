//
//  DetailVC.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/18.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher
import WebKit
//MARK: 영화 상세 VC
final class DetailVC: UIViewController {
    //MARK: - Properties
    let viewModel: DetailViewModel
    let disposeBag = DisposeBag()
    
    // NavigationBar
    let navigationBar = CustomNavigationBarView().then {
        $0.titleLabel.textAlignment = .center
        $0.leftBarButton.setImage(UIImage(named: "iconBack"), for: .normal)
        $0.leftBarButton.setImage(UIImage(named: "iconBack"), for: .highlighted)
        $0.leftBarButton.isHidden = false
    }
    // Navigation 구분선
    let navigationLineView = UIView().then {
        $0.backgroundColor = UIColor(named: "lineColor") ?? .lightGray
    }
    // 영화 포스터 이미지
    let posterImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFit
    }
    // 즐겨찾기 버튼
    let favoriteButton = UIButton().then {
        $0.setImage(UIImage(named: "iconStarEmpty"), for: .normal)
        $0.setImage(UIImage(named: "iconStarFilled"), for: .selected)
    }
    // 영화 감독
    let directorLabel = UILabel().then {
        $0.textAlignment = .left
        $0.font = .systemFont(ofSize: 15, weight: .regular)
    }
    // 출연진
    let actorsLabel = UILabel().then {
        $0.textAlignment = .left
        $0.font = .systemFont(ofSize: 15, weight: .regular)
    }
    // 평점
    let userRatingLabel = UILabel().then {
        $0.textAlignment = .left
        $0.font = .systemFont(ofSize: 15, weight: .regular)
    }
    // WebView
    lazy var movieWebView = WKWebView().then {
        $0.uiDelegate = self
        $0.navigationDelegate = self
    }
    
    //MARK: - Initializer
    init(viewModel: DetailViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        rxBind()
    }
    
    //MARK: - Configure UI
    func configureUI() {
        view.backgroundColor = .white
        
        view.addSubview(navigationBar)
        view.addSubview(navigationLineView)
        view.addSubview(posterImageView)
        view.addSubview(favoriteButton)
        view.addSubview(directorLabel)
        view.addSubview(actorsLabel)
        view.addSubview(userRatingLabel)
        view.addSubview(movieWebView)
        
        navigationBar.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(54)
        }
        
        navigationBar.titleLabel.snp.updateConstraints {
            $0.left.equalToSuperview().offset(54)
            $0.right.equalToSuperview().offset(-54)
        }
        
        navigationLineView.snp.makeConstraints {
            $0.top.equalTo(navigationBar.snp.bottom)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(10)
        }
        
        posterImageView.snp.makeConstraints {
            $0.top.equalTo(navigationLineView.snp.bottom).offset(5)
            $0.left.equalToSuperview().offset(20)
            $0.width.equalTo(60)
            $0.height.equalTo(90)
        }
        
        favoriteButton.snp.makeConstraints {
            $0.top.equalTo(navigationLineView.snp.bottom)
            $0.right.equalToSuperview()
            $0.size.equalTo(40)
        }
        
        directorLabel.snp.makeConstraints {
            $0.top.equalTo(posterImageView.snp.top).offset(5)
            $0.left.equalTo(posterImageView.snp.right).offset(10)
            $0.right.equalTo(favoriteButton.snp.left).offset(-5)
        }
        
        actorsLabel.snp.makeConstraints {
            $0.centerY.equalTo(posterImageView)
            $0.left.equalTo(posterImageView.snp.right).offset(10)
            $0.right.equalTo(favoriteButton.snp.left).offset(-5)
        }
        
        userRatingLabel.snp.makeConstraints {
            $0.bottom.equalTo(posterImageView.snp.bottom).offset(-5)
            $0.left.equalTo(posterImageView.snp.right).offset(10)
            $0.right.equalTo(favoriteButton.snp.left).offset(-5)
        }
        
        movieWebView.snp.makeConstraints {
            $0.top.equalTo(posterImageView.snp.bottom).offset(10)
            $0.left.right.bottom.equalToSuperview()
        }
    }
    
    //MARK: - Rx Binding..
    func rxBind() {
        // => 영화 제목
        viewModel
            .fetchTitle()
            .observe(on: MainScheduler.asyncInstance)
            .bind(to: navigationBar.titleLabel.rx.text)
            .disposed(by: disposeBag)
        // => 영화 감독
        viewModel
            .fetchDirector()
            .observe(on: MainScheduler.asyncInstance)
            .bind(to: directorLabel.rx.text)
            .disposed(by: disposeBag)
        // => 영화 출연진
        viewModel
            .fetchActors()
            .observe(on: MainScheduler.asyncInstance)
            .bind(to: actorsLabel.rx.text)
            .disposed(by: disposeBag)
        // => 영화 평점
        viewModel
            .fetchRating()
            .observe(on: MainScheduler.asyncInstance)
            .bind(to: userRatingLabel.rx.text)
            .disposed(by: disposeBag)
        // => 영화 포스터
        viewModel
            .fetchImageUrl()
            .observe(on: MainScheduler.asyncInstance)
            .bind(with: self, onNext: { owner, url in
                guard let URL = url else { return }
                owner.posterImageView.kf.setImage(with: URL)
            })
            .disposed(by: disposeBag)
        // => 즐겨찾는 영화
        viewModel
            .fetchIsFavorite()
            .bind(to: favoriteButton.rx.isSelected)
            .disposed(by: disposeBag)
        // => Link
        viewModel
            .fetchRequest()
            .observe(on: MainScheduler.asyncInstance)
            .bind(with: self, onNext: { owner, req in
                guard let request = req else { return }
                print(request)
                owner.movieWebView.load(request)
            })
            .disposed(by: disposeBag)
        // pop event
        navigationBar.leftBarButton
            .rx
            .tap
            .observe(on: MainScheduler.asyncInstance)
            .bind(with: self, onNext: { owner, _ in
                owner.navigationController?.popViewController(animated: true)
            })
            .disposed(by: disposeBag)
        // favorite button handler
        favoriteButton
            .rx
            .tap
            .bind(with: self, onNext: { owner, _ in
                // 즐겨찾기 추가/삭제 버튼 이미지 변경
                owner.favoriteButton.isSelected.toggle()
                // 즐겨찾기 추가/삭제
                owner.viewModel
                    .requestFavorite()
            })
            .disposed(by: disposeBag)
    }
}

//MARK: WKWebView Delegate
extension DetailVC: WKUIDelegate, WKNavigationDelegate {
}
