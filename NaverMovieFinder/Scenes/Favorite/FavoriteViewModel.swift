//
//  FavoriteViewModel.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/19.
//

import RxSwift
import RxCocoa
//MARK: - FavoriteViewModel
final class FavoriteViewModel {
    //MARK: Properties
    let input = Input()
    let output = Output()
    private let disposeBag = DisposeBag()
    private var movies = [FavoriteMovie]() { didSet {
        print(movies)
        output.favoriteMovies.accept(movies) } }
    
    //MARK: Initializer
    init() {
        rxBind()
    }
    
    //MARK: Rx Binding..
    func rxBind() {
        // <= 즐겨찾는 리스트 가져오기
        input
            .requestFavorites
            .bind(with: self, onNext: { owner, _ in
                owner.requestFavoriteList()
            })
            .disposed(by: disposeBag)
        // <= 즐겨찾기 삭제
        input
            .requestDeleteFavorites
            .bind(with: self, onNext: { owner, idx in
                owner.requestDeleteFavorite(idx: idx)
            })
            .disposed(by: disposeBag)
    }
    
    //MARK: Functions..
    // 즐겨찾는 영화목록 가져오기
    private func requestFavoriteList() {
        guard let list = try? FavoriteMovie.getFavorite() else {
            output.error.accept(ErrorResult.failLoadedList)
            movies = []
            return
        }
        movies = list
    }
    
    // <= 즐겨찾기 추가/삭제
    private func requestDeleteFavorite(idx: Int) {
        guard movies.count > idx else { return }
        let movie = movies[idx]

        do {
            try FavoriteMovie.deleteBy(id: movie.id)
            requestFavoriteList()
        } catch {
            output.error.accept(ErrorResult.failDelete)
        }
    }
}
//MARK: - Functions..
extension FavoriteViewModel {
    enum ErrorResult: Error {
        case failLoadedList
        case failDelete
    }
    
    struct Input {
        let requestFavorites = PublishRelay<Void?>() // <= 즐겨찾는 영화
        let requestDeleteFavorites = PublishRelay<Int>() // <= 즐겨찾는 영화
    }
    
    struct Output {
        let error = PublishRelay<Error>() // => Error
        let favoriteMovies = PublishRelay<[FavoriteMovie]>() // => 즐겨찾는 영화들
    }
}

