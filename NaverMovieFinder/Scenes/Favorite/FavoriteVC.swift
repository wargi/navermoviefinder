//
//  FavoriteVC.swift
//  NaverMovieFinder
//
//  Created by wargi_p on 2021/12/19.
//

import UIKit
import RxSwift
import RxCocoa
import Then
import SnapKit
//MARK: 즐겨찾는 영화 목록 VC
final class FavoriteVC: UIViewController {
    //MARK: - Properties
    let viewModel: FavoriteViewModel
    let disposeBag = DisposeBag()
    
    // NavigationBar
    let navigationBar = CustomNavigationBarView().then {
        $0.titleLabel.text = "즐겨찾기 목록"
        $0.titleLabel.textAlignment = .center
        $0.leftBarButton.setImage(UIImage(named: "iconClose"), for: .normal)
        $0.leftBarButton.setImage(UIImage(named: "iconClose"), for: .highlighted)
        $0.leftBarButton.isHidden = false
    }
    // 즐겨찾는 영화 리스트 TableView
    let favoritesTableView = UITableView().then {
        $0.alwaysBounceVertical = false
        $0.alwaysBounceHorizontal = false
        $0.showsVerticalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
        $0.register(SearchMovieTableViewCell.self, forCellReuseIdentifier: SearchMovieTableViewCell.identifier)
    }
    // 즐겨찾는 영화가 없을 때 표시할 label
    let emptyLabel = UILabel().then {
        $0.text = "즐겨찾기한 영화가 없습니다."
        $0.textColor = .darkGray
        $0.textAlignment = .center
        $0.font = .systemFont(ofSize: 12, weight: .light)
    }
    
    //MARK: - Initializer
    init(viewModel: FavoriteViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        rxBind()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.input.requestFavorites.accept(nil)
    }
    
    //MARK: - Configure UI
    func configureUI() {
        navigationController?.navigationBar.isHidden = true
        
        view.backgroundColor = .white
        
        view.addSubview(navigationBar)
        view.addSubview(favoritesTableView)
        view.addSubview(emptyLabel)
        
        navigationBar.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.top)
            $0.left.right.equalToSuperview()
            $0.height.equalTo(54)
        }
        
        navigationBar.titleLabel.snp.updateConstraints {
            $0.left.equalToSuperview().offset(54)
            $0.right.equalToSuperview().offset(-54)
        }
        
        favoritesTableView.snp.makeConstraints {
            $0.top.equalTo(navigationBar.snp.bottom).offset(10)
            $0.left.bottom.equalToSuperview()
            $0.right.equalToSuperview().offset(-20)
            $0.bottom.equalToSuperview()
        }
        
        emptyLabel.snp.makeConstraints {
            $0.centerY.equalTo(favoritesTableView)
            $0.left.right.equalToSuperview()
        }
    }
    
    //MARK: - Rx Binding..
    func rxBind() {
        // left barbutton event handler
        navigationBar
            .leftBarButton
            .rx
            .tap
            .observe(on: MainScheduler.asyncInstance)
            .bind(with: self, onNext: { owner, _ in
                // dismiss
                owner.dismiss(animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
        // Server => 검색 결과 리스트 표시
        viewModel
            .output
            .favoriteMovies
            .map { movies -> [Movie] in // DB Movie => Movie로 변환
                return movies.map {
                    Movie(title: $0.title,
                          link: $0.link,
                          image: $0.imaegeUrlString,
                          subtitle: nil,
                          pubDate: nil,
                          director: $0.director,
                          actor: $0.actors,
                          userRating: $0.rating)
                }
            }
            .bind(to: favoritesTableView.rx.items(cellIdentifier: SearchMovieTableViewCell.identifier,
                                               cellType: SearchMovieTableViewCell.self)) { row, info, cell in
                cell.fetch(movie: info)
                cell.favoriteButton.tag = row
                cell.favoriteButton.addTarget(self, action: #selector(self.cellFavoriteTapped(sender:)), for: .touchUpInside)
                cell.favoriteButton.isSelected = true
            }
            .disposed(by: disposeBag)
        // cell 선택 event handler
        favoritesTableView
            .rx
            .modelSelected(Movie.self)
            .bind(with: self, onNext: { owner, info in
                // FavoriteVC => DetailVC
                let detailViewModel = DetailViewModel(movie: info)
                let detailVC = DetailVC(viewModel: detailViewModel)
                owner.navigationController?.pushViewController(detailVC, animated: true)
            })
            .disposed(by: disposeBag)
        // 내역이 없을 때 내역 없음 label 표시
        viewModel
            .output
            .favoriteMovies
            .map { !$0.isEmpty }
            .bind(to: emptyLabel.rx.isHidden)
            .disposed(by: disposeBag)
    }
    
    // Cell Favorite button tap handler
    @objc
    func cellFavoriteTapped(sender: UIButton) {
        // <= 즐겨찾기 삭제
        viewModel
            .input
            .requestDeleteFavorites
            .accept(sender.tag)
    }
}
